// Tools
const path = require("path");

// Plugins
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

// Variables - Directory Target
const dir = path.resolve(__dirname);
const srcDir = path.join(dir, "src");
const distDir = path.join(dir, "dist");
const nodeModulesDir = path.join(dir, "node_modules");

// Variables - File Targets
const srcIndex = "index.ts";

// Variables - Environment
const webpackMode = process.env.NODE_ENV === "production" ? "production" : "development";
const isProduction = webpackMode === "production";

// Main Webpack Config
module.exports = {
  target: "node",
  mode: webpackMode,
  entry: path.join(srcDir, srcIndex),
  output: {
    path: path.resolve(distDir),
    filename: "[name].js",
  },
  resolve: {
    alias: [],
    extensions: [".js", ".ts", ".json"],
    mainFields: ["main", "module"],
    plugins: [new TsconfigPathsPlugin({ configFile: path.join(dir, "tsconfig.json") })],
  },
  module: {
    rules: [
      {
        test: /.(ts|tsx)$/,
        use: "ts-loader",
        include: dir,
        exclude: [nodeModulesDir, /.test.tsx?$/],
      },
    ],
  },
  plugins: [],
};
