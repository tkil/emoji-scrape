import { fetchUnicodeDom } from "./api/unicodeOrg";
import jsdom from "jsdom";
import { parseDomAndBuildJsonData } from "./scrapeLogic";

const main = async () => {
  const result = await fetchUnicodeDom();

  if (result.status === "success") {
    const window = new jsdom.JSDOM(result.dom).window;
    const data = await parseDomAndBuildJsonData(window.document);
  } else {
    console.error("Could not grab dom, sorry");
  }
};

main();
