import fetch from "node-fetch";

export const unicodeOrgDomain = "http://unicode.org";

export const fetchUnicodeDom = async () => {
  try {
    const response = await fetch(`${unicodeOrgDomain}/emoji/charts/full-emoji-list.html`, {
      method: "get",
      headers: { "Content-Type": "application/json" },
    });
    return { dom: await response.text(), status: "success" };
  } catch (err) {
    console.error("Failed to fetch from unicode.org, Err: ", err);
    return { dom: undefined, status: "error" };
  }
};
